import requests


def get_starships_from_api(url):
    """Get starships from SWAPI

    Args:
        url (str): The API url
    
    Returns:
        result (dict): The startships with name and hyper
    """

    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        # Handling only http error for now
        print(f"HTTP error: {err}")
        return {}

    return response.json()
