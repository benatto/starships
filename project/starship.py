from flask_restful import Resource

from project.utils import get_starships_from_api
from project.constants import SWAPI


class StarShip(Resource):
    @staticmethod
    def _filtered_starships(starships):

        known_hyperdrive = []
        unknown_hyperdrive = []

        for starship in starships.get("results", {}):
            hyperdrive_rating = starship.get("hyperdrive_rating", "")
            name = starship.get("name", "")
            item = {}
            if not hyperdrive_rating:
                item["name"] = name
                unknown_hyperdrive.append(item)
            else:
                item["hyperdrive"] = float(hyperdrive_rating)
                item["name"] = name
                known_hyperdrive.append(item)
        known_hyperdrive.sort(key=lambda x: x["hyperdrive"])

        return {
            "starships": known_hyperdrive,
            "starships_unknown_hyperdrive": unknown_hyperdrive,
        }

    def get(self):
        starships = get_starships_from_api(SWAPI)
        return StarShip._filtered_starships(starships)
