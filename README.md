# Starships

Starships API.

## Table of Contents

* [How to build the service](#introduction)
* [How to run tests](#tests)
* [Example using API Endpoint](#example)


## How to build the service <a name="build"></a>

```
$ docker-compose up
```

# How to run tests <a name="tests"></a>

You should be able to run unit tests with docker.

```
$ docker-compose -f docker-compose.testing.yml run tests docker/scripts/tests
```

# Example using API Endpoint <a name="example"></a>

Example of API response:

http://localhost:5000/api/v1/starships

```
{
    "starships": [
        {
            "hyperdrive": 0.5,
            "name": "Millennium Falcon"
        },
        {
            "hyperdrive": 1.0,
            "name": "Sentinel-class landing craft"
        },
        {
            "hyperdrive": 1.0,
            "name": "Y-wing"
        },
        {
            "hyperdrive": 1.0,
            "name": "X-wing"
        },
        {
            "hyperdrive": 1.0,
            "name": "TIE Advanced x1"
        },
        {
            "hyperdrive": 1.0,
            "name": "Imperial shuttle"
        },
        {
            "hyperdrive": 2.0,
            "name": "Executor"
        },
        {
            "hyperdrive": 2.0,
            "name": "EF76 Nebulon-B escort frigate"
        },
        {
            "hyperdrive": 3.0,
            "name": "Slave 1"
        },
        {
            "hyperdrive": 4.0,
            "name": "Death Star"
        }
    ],
    "starships_unknown_hyperdrive": []
}
```
