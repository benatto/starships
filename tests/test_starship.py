from flask import json

from app import app

from mock import patch, Mock

app.testing = True
app.debug = True


@patch("project.utils.requests.get")
def test_if_returns_empty_when_swapi_returns_empty(mock_get: Mock):
    mock_get.return_value.ok = True
    mock_get.return_value.json.return_value = {}

    response = app.test_client().get("/api/v1/starships")
    response_json = json.loads(response.get_data(as_text=True))

    assert len(response_json["starships"]) == 0
    assert len(response_json["starships_unknown_hyperdrive"]) == 0

    assert response.status_code == 200


@patch("project.utils.requests.get")
def test_api_when_returns_result_only_with_starships(mock_get: Mock):
    mock_get.return_value.ok = True
    mock_get.return_value.json.return_value = {
        "results": [
            {"name": "Executor", "hyperdrive_rating": "2.0"},
            {"name": "Sentinel-class landing craft", "hyperdrive_rating": "1.0"},
        ]
    }

    response = app.test_client().get("/api/v1/starships")
    response_json = json.loads(response.get_data(as_text=True))

    assert len(response_json["starships"]) == 2
    assert len(response_json["starships_unknown_hyperdrive"]) == 0

    assert response.status_code == 200


@patch("project.utils.requests.get")
def test_when_api_returns_only_results_with_starships_unknown_hyperdrive(
    mock_get: Mock
):
    mock_get.return_value.ok = True
    mock_get.return_value.json.return_value = {
        "results": [{"name": "Executor"}, {"name": "Sentinel-class landing craft"}]
    }

    response = app.test_client().get("/api/v1/starships")
    response_json = json.loads(response.get_data(as_text=True))

    assert len(response_json["starships"]) == 0
    assert len(response_json["starships_unknown_hyperdrive"]) == 2

    assert response.status_code == 200


@patch("project.utils.requests.get")
def test_if_api_returns_ordered_by_hiperdrive(mock_get: Mock):
    mock_get.return_value.ok = True
    mock_get.return_value.json.return_value = {
        "results": [
            {"name": "Executor", "hyperdrive_rating": "2.0"},
            {"name": "Sentinel-class landing craft", "hyperdrive_rating": "1.0"},
            {"name": "Death Star", "hyperdrive_rating": "4.0"},
            {"name": "Millennium Falcon", "hyperdrive_rating": "0.5"},
        ]
    }

    response = app.test_client().get("/api/v1/starships")
    response_json = json.loads(response.get_data(as_text=True))

    assert response_json["starships"][0]["name"] == "Millennium Falcon"
    assert response_json["starships"][0]["hyperdrive"] == 0.5

    assert response_json["starships"][-1]["name"] == "Death Star", "hyperdrive_rating"
    assert response_json["starships"][-1]["hyperdrive"] == 4.0

    assert response.status_code == 200
