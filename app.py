import os

from flask import Flask, abort, jsonify
from flask_restful import Api


from project.starship import StarShip


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_object(config)

    return app


app = create_app()

api = Api(app)
api.add_resource(StarShip, "/api/v1/starships")


@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
